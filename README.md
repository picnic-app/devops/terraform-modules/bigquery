<!-- BEGIN_TF_DOCS -->
# Terraform BigQuery module

Creates BigQuery dataset and IAM

## How to release new version

1. Update CHANGELOG.md
1. Merge your changes into master
1. Create new semver tag

## Usage

You can find usage example in any [uploaded package](https://gitlab.com/picnic-app/devops/terraform-modules/bigquery/-/infrastructure_registry)

```terraform
module "bigquery" {
  source = "gitlab.com/picnic-app/bigquery/google"
  version = "~> 1.0"

  env_id       = var.env_id
  service_name = var.service_name
  sa_email     = module.workload-identity.service_account.email

  tables = {
    test = {
      schema = ""
    }
  }
}
```

## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.3 |
| <a name="requirement_google"></a> [google](#requirement\_google) | ~> 4.33 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_google"></a> [google](#provider\_google) | ~> 4.33 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [google_bigquery_dataset.this](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/bigquery_dataset) | resource |
| [google_bigquery_dataset_iam_member.this](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/bigquery_dataset_iam_member) | resource |
| [google_bigquery_table.this](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/bigquery_table) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_env_id"></a> [env\_id](#input\_env\_id) | ID of environment | `string` | n/a | yes |
| <a name="input_sa_email"></a> [sa\_email](#input\_sa\_email) | Email of GCP SA for service | `string` | n/a | yes |
| <a name="input_service_name"></a> [service\_name](#input\_service\_name) | Name of service to be deployed | `string` | n/a | yes |
| <a name="input_tables"></a> [tables](#input\_tables) | Map for defining BigQuery tables. Map keys are table names | <pre>map(object({<br>    schema = any<br>  }))</pre> | `{}` | no |

## Outputs

No outputs.
<!-- END_TF_DOCS -->