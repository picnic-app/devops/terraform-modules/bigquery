# BigQuery module

## [1.0.1] - 2023-06-22
- Disable deletion protection for non-prod environments

## [1.0.0] - 2023-06-20
- Initial release of the module.
