variable "env_id" {
  type        = string
  description = "ID of environment"
}

variable "service_name" {
  type        = string
  description = "Name of service to be deployed"
}

variable "sa_email" {
  type        = string
  description = "Email of GCP SA for service"
}

variable "tables" {
  type = map(object({
    schema = any
  }))
  default     = {}
  description = "Map for defining BigQuery tables. Map keys are table names"
}
