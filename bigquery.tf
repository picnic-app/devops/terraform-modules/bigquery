locals {
  is_prod    = var.env_id == "prod"
  dataset_id = replace("${var.service_name}_${var.env_id}", "-", "_")
}

resource "google_bigquery_dataset" "this" {
  dataset_id  = local.dataset_id
  description = "Managed by Terraform"

  location                   = local.is_prod ? "US" : "europe-west3"
  is_case_insensitive        = false
  delete_contents_on_destroy = true

  labels = {
    environment = var.env_id
  }
}

resource "google_bigquery_table" "this" {
  for_each = var.tables

  dataset_id  = google_bigquery_dataset.this.dataset_id
  table_id    = each.key
  description = "Managed by Terraform"

  deletion_protection = local.is_prod
  schema              = jsonencode(each.value.schema)

  labels = {
    environment = var.env_id
  }
}

resource "google_bigquery_dataset_iam_member" "this" {
  dataset_id = google_bigquery_dataset.this.dataset_id
  role       = "roles/bigquery.dataEditor"
  member     = "serviceAccount:${var.sa_email}"
}
