/**
 * # Terraform BigQuery module
 *
 * Creates BigQuery dataset and IAM
 *
 * ## How to release new version
 *
 * 1. Update CHANGELOG.md
 * 1. Merge your changes into master
 * 1. Create new semver tag
 *
 * ## Usage
 *
 * You can find usage example in any [uploaded package](https://gitlab.com/picnic-app/devops/terraform-modules/bigquery/-/infrastructure_registry)
 *
 * ```terraform
 * module "bigquery" {
 *   source = "gitlab.com/picnic-app/bigquery/google"
 *   version = "~> 1.0"
 *
 *   env_id       = var.env_id
 *   service_name = var.service_name
 *   sa_email     = module.workload-identity.service_account.email
 *
 *
 *   tables = {
 *     test = {
 *       schema = ""
 *     }
 *   }
 * }
 * ```
 */
